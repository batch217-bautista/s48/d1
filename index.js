// console.log("Hello World!");

// it's array because we can add multiple text/strings
let posts = [];
let count = 1;

// ADD POST DATA

document.querySelector("#form-add-post").addEventListener('submit', (e) => {

    e.preventDefault(); // to stop the page from refreshing..try removing it

    posts.push({
        id: count,
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    })

    count++;
    // count = count + 1;

    showPost(posts); // Hoisting

    alert("Successfully added");
})

// SHOW POST

const showPost = (post) => {
    let postEntries = "";

    // used forEach to have all the text/strings be shown on the post tab
    posts.forEach((post) => {
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>

                <p id="post-body-${post.id}">${post.body}</p>

                <button onclick="editPost('${post.id}')">Edit</button>

                <button onclick="deletePost('${post.id}')">Delete</button>
            </div>`;
    });

    document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// EDIT POST

const editPost = (id) => {
    // used #post-title-${id} and #post-body-${id} because that's what being used on the SHOW POST
    let title = document.querySelector(`#post-title-${id}`).innerHTML;

    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = id;

    document.querySelector("#txt-title-edit").value = title;

    document.querySelector("#txt-body-edit").value = body;
}

// UPDATE POST

document.querySelector("#form-edit-post").addEventListener('submit', (e) => {

    e.preventDefault();

    // for-loop was used because it needs to compare if there's any text/strings inside it(after adding the text)
    for(let i = 0; i < posts.length; i++) {
        if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
            posts[i].title = document.querySelector("#txt-title-edit").value;

            posts[i].body = document.querySelector("#txt-body-edit").value;

            // to show the updated post
            showPost(posts);
            alert("Successfully updated");

            break;
        }
    }
})

// DELETE POST

const deletePost = (id) => {
    posts = posts.filter((post) => {
        if(post.id.toString() !== id) {
            return post;
        }
    })

    document.querySelector(`#post-${id}`).remove();
    alert("Text removed successfully"); // I just added this ahaha
}